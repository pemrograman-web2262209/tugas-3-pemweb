<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>FORM</title>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
</head>
<body>
    <h2>FORM</h2>

    <form id="myForm">
        <label for="name">Nama:</label><br>
        <input type="text" id="name" name="name"><br>

        <label for="gender">Jenis Kelamin:</label><br>
        <input type="radio" id="male" name="gender" value="Cowok">
        <label for="male">Cowok</label><br>
        <input type="radio" id="female" name="gender" value="Cewek">
        <label for="female">Cewek</label><br>

        <label for="city">Kota Asal:</label><br>
        <select id="city" name="city">
            <option value="Jakarta">Jakarta</option>
            <option value="Malang">Malang</option>
            <option value="Jakarta">Jakarta</option>
            <option value="Tidak dari bumi">Tidak dari bumi</option>
        </select><br>

        <input type="submit" value="Submit">
    </form>

    <div id="response"></div>

    <script>
    $("#myForm").submit(function(e) {
        e.preventDefault();

        $.ajax({
            type: "POST",
            url: "t2[225150400111051].php",
            data: $("#myForm").serialize(),
            success: function(data) {
                $("#response").html(data);
            }
        });
    });
    </script>
</body>
</html>